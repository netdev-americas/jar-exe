(ns austin.core-test
  (:use austin.core 
        tupelo.core 
        clojure.test )
  (:require 
    [clojure.java.io :as io]
    [clojure.java.shell :as shell]
    [tupelo.misc :as tm]
  ))
(spyx *clojure-version*)

(newline)
(def unique-service-name  "austin")  ; NOTE: will be a subdir of "/tmp"
(def exe-name             "hello")
(def resource-name        "hello")

(def ^:dynamic *os-shell* "/bin/bash")  ; could also use /bin/zsh, etc
(defn shell-run
  "Run a command represented as a string in an OS shell (default=/bin/bash).
  Example: 'ls -ldF *'  "
  [& cmds]
  (let [cmd-str   (apply glue cmds)
        result    (shell/sh *os-shell* "-c" cmd-str) ]
    (if (= 0 (grab :exit result))
      (grab :out result)
      (throw (RuntimeException. 
               (str "shell-cmd: clojure.java.shell/sh failed. \n" 
                    "cmd-str:"     cmd-str             "\n"
                    "exit status:" (grab :exit result) "\n"
                    "stderr:"      (grab :err  result) "\n"
                    "result:"      (grab :out  result) "\n" ))))))

(assert (truthy? (re-matches #"[a-z]+" unique-service-name)))  ; verify it is a simple name
(def unique-work-dir  (str "/tmp/" unique-service-name))
(def exe-abs-path     (str unique-work-dir "/" exe-name))
(spyx exe-abs-path)

(shell-run   "mkdir -p  " unique-work-dir)
(shell-run   "chmod 700 " unique-work-dir)
(print 
  (shell-run "ls -ldF   " unique-work-dir))
(shell-run   "rm -f     " exe-abs-path)

(io/copy (io/file (io/resource resource-name)) (io/file exe-abs-path))

(print (shell-run "ls -ldF   " exe-abs-path))
(shell-run        "chmod 700 " exe-abs-path)
(print (shell-run "ls -ldF   " exe-abs-path))
(print (shell-run exe-abs-path))
